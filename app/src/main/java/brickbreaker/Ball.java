package brickbreaker;

import java.awt.Color;
import java.awt.Graphics;

public class Ball {
    private Vector position;
    private Vector direction;
    public int radius;
    public Color color;
    
    public Ball(int posX, int posY, int dirX, int dirY, int radius, Color color) {
        this.position = new Vector(posX, posY);
        this.direction = new Vector(dirX, dirY);
        this.radius = radius;
        this.color = color;
    }
    
    public void setPosition(int posX, int posY) {
        this.position = new Vector(posX, posY);
    }
    
    public void setDirection(int dirX, int dirY) {
        this.direction = new Vector(dirX, dirY);
    }
    
    public Vector getPosition() {
        return this.position;
    }
    
    public int getRadius() {
        return this.radius;
    }
    
    public Color getColor() {
        return this.color;
    }
    
    public Vector getDirection() {
        return this.direction;
    }
    
    public void move(){
        position.x += direction.x;
        position.y += direction.y;
    }
    
    public void checkForWallCollisions() {
        if (position.x <= 0) { // Left wall collision
            position.x = 0; // Set the ball at the left edge
            direction.x = Math.abs(direction.x); // Reverse the x direction
        }

        if (position.x + radius >= 680) { // Right wall collision
            position.x = 680 - radius; // Set the ball at the right edge
            direction.x = -Math.abs(direction.x); // Reverse the x direction
        }

        if (position.y <= 0) { // Top wall collision
            position.y = 0; // Set the ball at the top edge
            direction.y = Math.abs(direction.y); // Reverse the y direction
        }
    }
    
    public void draw(Graphics graphics) {
        graphics.setColor(this.color); // ball color
        graphics.fillOval(position.x, position.y, radius, radius);
    }
}
