
package brickbreaker.GameComponents;

import brickbreaker.GameComponents.Brick;
import brickbreaker.Interfaces.BrickFactory;
import java.awt.Color;

public class DefaultBrickFactory implements BrickFactory {
    @Override
    public Brick createBrick(int x, int y, int width, int height, Boolean hasPowerUp) {
        Color color = Color.decode("#BC4A3C");
        if(hasPowerUp)
            color = Color.decode("#FF0000");
        return new Brick(x, y, width, height, color, hasPowerUp);
    }
}