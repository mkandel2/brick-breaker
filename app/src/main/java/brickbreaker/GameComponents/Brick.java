
package brickbreaker.GameComponents;

import brickbreaker.Vector;
import java.awt.Color;
import java.awt.Graphics2D;

public class Brick {
    private Vector position;
    private int width;
    private int height;
    private Color color;
    public Boolean hasPowerUp;
    private final int value = 5;
    private boolean isHit = false;

    public Brick(int x, int y, int width, int height, Color color, Boolean hasPowerUp) {
        this.position = new Vector(x, y);
        this.width = width;
        this.height = height;
        this.color = color;
        this.hasPowerUp = hasPowerUp;
    }

    public void draw(Graphics2D g) {
        g.setColor(this.color); // brick color
        g.fillRect(this.position.x, this.position.y, width, height);

        g.setColor(Color.BLACK);
        g.drawRect(this.position.x, this.position.y, width, height);
    }
    
    public void setIsHit(boolean isHit) {
        this.isHit = isHit;
    }
    
    public boolean getIsHit() {
        return isHit;
    }
    
    
    public int getValue() {
        return value;
    }
    
    public Vector getPosition() {
        return position;
    }
    
    public int getHeight() {
        return height;
    }
    
    public int getWidth() {
        return width;
    }
}
