
package brickbreaker;

import Auth.User;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class App {
    public static User user;
    public static final String DB_URL = "jdbc:mysql://localhost:3306/brick_breaker";
    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "";
    public static Menu mainMenu;
    public static GamePanel gamePanel;
    public static ScoringSystem scoreSystem = new ScoringSystem();
    public static JFrame frame;
    public static HighScores highScorePanel;
    public static Register registrationPanel;
    
    public static void main(String[] args) {
        frame = new JFrame();
        frame.setBounds(10, 10, 700, 600);
        frame.setTitle("Brick Breaker");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainMenu = new Menu(frame);
        gamePanel = new GamePanel(frame);
        
        frame.pack();

        frame.setVisible(true);
    }
    
    public static void createGame() {
        App.gamePanel = new GamePanel(frame);
    }
    
    public static void createMainMenu() {
        App.mainMenu = new Menu(frame);
    }
    
    public static void createHighScorePanel() {
        List<Integer> scores = scoreSystem.getScores();
        App.highScorePanel = new HighScores(scores);
    }
    
    public static void createRegistrationPanel() {
        App.registrationPanel = new Register(frame);
    }
}
