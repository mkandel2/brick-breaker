/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package brickbreaker;

import brickbreaker.Interfaces.GameRenderer;
import java.awt.*;
import java.util.Iterator;
import java.util.List;
import javax.swing.JPanel;

public class DefaultGameRenderer implements GameRenderer {
    @Override
    public void render(
        GamePanel gamePanel,
        Graphics graphics,
        List<Ball> balls,
        int paddle,
        ScoringSystem scoringSystem,
        int totalBricks
    ) {
        if (balls.isEmpty()) {
            gamePanel.gameOver(graphics, "Game Over", Color.BLACK);
            return;
        }

        if (!gamePanel.play) {
            graphics.setFont(new Font(gamePanel.font, Font.BOLD, 20));
            graphics.drawString("Press Enter to Start", 230, 350);
            return;
        }
        
        // Background color
        graphics.setColor(Color.green);
        graphics.fillRect(1, 1, 692, 592);

        // Render game map
        gamePanel.gameMap.draw((Graphics2D) graphics);

        // Render paddle
        graphics.setColor(Color.blue);
        graphics.fillRect(paddle, 550, 100, 20);

        Iterator<Ball> ballsIterator = balls.iterator();

        while (ballsIterator.hasNext()) {
            Ball ball = ballsIterator.next();
            Vector ballPosition = ball.getPosition();

            if (ballPosition.y > 560) {
                ballsIterator.remove();
                continue;
            } else {
                ball.checkForWallCollisions();
            }

            ball.draw(graphics);
        }

        // Render score
        graphics.setColor(Color.black);
        graphics.setFont(new Font("MV Boli", Font.BOLD, 25));
        graphics.drawString("Score: " + scoringSystem.getScore(), 520, 30);

        // Render high score
        graphics.setColor(Color.black);
        graphics.setFont(new Font("MV Boli", Font.BOLD, 25));
        graphics.drawString("High Score: " + scoringSystem.getHighScore(), 10, 30);

        if (totalBricks <= 0) {
            Color color = new Color(0XFF6464);
            gamePanel.gameOver(graphics, "You Won", color);
        }

        if (balls.isEmpty()) {
            gamePanel.gameOver(graphics, "Game Over", Color.BLACK);
        }
    }
}
