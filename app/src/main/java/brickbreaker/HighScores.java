
package brickbreaker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class HighScores extends JPanel {
    private List<Integer> scores;

    public HighScores(List<Integer> scores) {
        this.scores = scores;
        initializeUI();
    }

    public void setScores(List<Integer> scores) {
        this.scores = scores;
    }

    private void initializeUI() {
        setLayout(new BorderLayout());

        // Create a JLabel to display the title
        JLabel titleLabel = new JLabel("High Scores");
        titleLabel.setFont(new Font("Arial", Font.BOLD, 24));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        add(titleLabel, BorderLayout.NORTH);

        // Create a JTable to display the scores
        JTable scoresTable = new JTable() {
        @Override
        public boolean isCellEditable(int row, int column) {
                return false; // Disable cell editing
            }
        };
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.addColumn("S/N");
        tableModel.addColumn("Score");

        int rank = 1;
        for (int score : scores) {
            tableModel.addRow(new Object[]{rank++, score});
        }

        scoresTable.setModel(tableModel);
        scoresTable.setFont(new Font("Arial", Font.PLAIN, 16));
        scoresTable.getTableHeader().setFont(new Font("Arial", Font.BOLD, 16));
        scoresTable.setRowHeight(30);

        // Add the scoresTable to a JScrollPane for scrolling
        JScrollPane scrollPane = new JScrollPane(scoresTable);
        add(scrollPane, BorderLayout.CENTER);

        JButton backButton = new JButton("Back");
        backButton.setFont(new Font("Arial", Font.PLAIN, 16));

        // Add an ActionListener to handle the back button click
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                App.frame.remove(HighScores.this);
                App.mainMenu.show();
                App.frame.revalidate();
                App.frame.repaint();
            }
        });

        // Add the back button to the bottom of the panel
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(backButton);
        add(buttonPanel, BorderLayout.SOUTH);
    }
}
