
package brickbreaker;

import brickbreaker.GameComponents.Brick;
import brickbreaker.Interfaces.BrickFactory;
import java.awt.Graphics2D;

public class GameMapBuilder {
	
	public Brick map [][];
	public int brickWidth = 50;
	public int brickHeight = 20;
	private BrickFactory brickfactory;
        private final int maxNoOfPowerUpsPerRow = 5;
        
	public GameMapBuilder(int row, int col, BrickFactory brickFactory) {
            map = new Brick [row][col];
            this.brickfactory = brickFactory;

            for (int i = 0; i < map.length; i++) { 
                int numberOfPowerUps = (int) (Math.random() * maxNoOfPowerUpsPerRow );
                for (int j=0; j< map[0].length;j++) {
             
                    boolean hasPowerUp = false;
                    if(numberOfPowerUps > 0) {
                        hasPowerUp = Math.random() < 0.5;
                    }
                    if(hasPowerUp) {
                        numberOfPowerUps--;
                    }
                    Brick brick = brickfactory.createBrick(j*brickWidth, i*brickHeight, brickWidth, brickHeight, hasPowerUp);
                    
                    map[i][j] = brick;
                }
            }
            
	}
	
	public void draw(Graphics2D g) {
            
            for (int i = 0; i < map.length; i++) {
                for (int j=0; j< map[0].length;j++) {
                    Brick brick = map[i][j];
                    if(!brick.getIsHit()) {
                        brick.draw(g);
                    }
                }
            }
	}

}