package brickbreaker;

import brickbreaker.Interfaces.AuthenticationService;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginFrame extends JPanel {
    public JTextField emailField;
    public JPasswordField passwordField;
    public JButton submitButton;
    public JButton backButton;
    public JLabel headerLabel, usernameLabel, passwordLabel;
    public JFrame parentFrame;
    public Color buttonColor = new Color(36, 172, 226);
    public Color hoverButtonColor = new Color(26, 152, 206);
    public Font textFont = new Font("MV Boli", Font.PLAIN, 14);
    public Font labelFont = new Font("MV Boli", Font.BOLD, 16);
    public Font headerFont = new Font("MV Boli", Font.BOLD, 24);
    public JLabel emailErrorLabel = new JLabel("");
    public JLabel passwordErrorLabel = new JLabel("");
    public AuthenticationService auth;
    public LoginFrame(JFrame parentFrame) {
        this.parentFrame = parentFrame;
        initComponents();
        setBackground(new Color(240, 240, 255)); // Light blue background
    }

    private void initComponents() {
        
        headerLabel = new JLabel("Login to Brick Breaker");
        headerLabel.setFont(headerFont);
        headerLabel.setForeground(buttonColor);

        usernameLabel = new JLabel("Email:");
        passwordLabel = new JLabel("Password:");

        emailField = new JTextField(20);
        passwordField = new JPasswordField(20);

        submitButton = new JButton("Login");
        backButton = new JButton("Back");

        usernameLabel.setFont(labelFont);
        passwordLabel.setFont(labelFont);
        emailField.setFont(textFont);
        passwordField.setFont(textFont);
        submitButton.setFont(new Font("MV Boli", Font.BOLD, 16));
        backButton.setFont(new Font("MV Boli", Font.BOLD, 16));

        submitButton.setBackground(buttonColor);
        submitButton.setForeground(Color.WHITE);
        backButton.setBackground(buttonColor);
        backButton.setForeground(Color.WHITE);

        submitButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
        backButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
        
        emailErrorLabel.setPreferredSize(new Dimension(200, 20));
        passwordErrorLabel.setPreferredSize(new Dimension(200, 20));
        emailErrorLabel.setForeground(Color.RED);
        passwordErrorLabel.setForeground(Color.RED);

        // Hover effects for buttons
        submitButton.addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent evt) {
                submitButton.setBackground(hoverButtonColor);
            }

            public void mouseExited(MouseEvent evt) {
                submitButton.setBackground(buttonColor);
            }
        });
        
        backButton.addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent evt) {
                backButton.setBackground(hoverButtonColor);
            }

            public void mouseExited(MouseEvent evt) {
                backButton.setBackground(buttonColor);
            }
        });

        submitButton.addActionListener(e -> handleLogin());
        backButton.addActionListener(e -> handleBack());

        // Layout with padding and alignment
        setLayout(new GridBagLayout());
    GridBagConstraints gbc = new GridBagConstraints();

    gbc.insets = new Insets(10, 10, 10, 10);

    gbc.gridwidth = 2;
    gbc.gridx = 0;
    gbc.gridy = 0;
    add(headerLabel, gbc);

    gbc.gridwidth = 1;
    gbc.gridx = 0;
    gbc.gridy = 1;
    add(usernameLabel, gbc);

    gbc.gridx = 1;
    add(emailField, gbc);

    gbc.gridx = 2;
    add(emailErrorLabel, gbc);

    gbc.gridx = 0;
    gbc.gridy = 2;
    add(passwordLabel, gbc);

    gbc.gridx = 1;
    add(passwordField, gbc);

    gbc.gridx = 2;
    add(passwordErrorLabel, gbc);

    gbc.gridwidth = 2;
    gbc.gridx = 0;
    gbc.gridy = 3;
    add(submitButton, gbc);

    gbc.gridy = 4;
    add(backButton, gbc);

    setBorder(BorderFactory.createEmptyBorder(50, 50, 50, 50));
    }

    public void handleLogin() {
        // Reset error labels
        emailErrorLabel.setText("");
        passwordErrorLabel.setText("");

        String email = emailField.getText();
        String password = new String(passwordField.getPassword());
        boolean error = false;
        // Simple validation
        if (email.isEmpty()) {
            emailErrorLabel.setText("Email is required!");
            error = true;
        } else if(!isValidEmail(email)) {
            emailErrorLabel.setText("Invalid email format!");
            error = true;
        }

        if (password.isEmpty()) {
            passwordErrorLabel.setText("Password is required!");
            error = true;
        }

        if (!error && auth.login(email, password)) {
           App.scoreSystem.setHighScore();
           parentFrame.remove(this);
           if(App.gamePanel == null)
                App.createGame();
           App.gamePanel.show();
        } else {
            JOptionPane.showMessageDialog(this, "Invalid username or password!", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void handleBack() {
        parentFrame.remove(this);
        App.mainMenu.show();
    }
    
    public static boolean isValidEmail(String email) {
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    
    public void setAuthenticateInstance(AuthenticationService auth) {
        this.auth = auth;
    }
    
    public void setEmailFieldText(String text) {
        emailField.setText(text);
    }
    
    public void setPasswordFieldText(String text) {
        passwordField.setText(text);
    }
    
    public String getEmailErrorLabelText() {
        return emailErrorLabel.getText();
    }
    
    public String getPasswordErrorLabelText() {
        return passwordErrorLabel.getText();
    }
}

