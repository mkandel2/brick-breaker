/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package brickbreaker;

import Auth.Authenticate;
import brickbreaker.Interfaces.AuthenticationService;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.Timer;

public class Menu extends JPanel {
    private JButton guest;
    private JButton highScore;
    private JScrollPane jScrollPane1;
    private JButton loginButton;
    private JButton registerButton;
    private JTextPane welconText;
    private JFrame frame;
    private Timer animationTimer;
    private int textPosition = -200;
    
    public Menu(JFrame frame) {
        this.frame = frame;
        initComponents();
        initAnimation();
    }

    private void initAnimation() {
        animationTimer = new Timer(10, e -> {
            if (textPosition < 117) { // target Y position for the text
                textPosition += 2;
                jScrollPane1.setLocation(jScrollPane1.getX(), textPosition);
            } else {
                animationTimer.stop();
            }
            repaint();
        });
        animationTimer.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        // Gradient background
        Graphics2D g2d = (Graphics2D) g;
        Color color1 = new Color(36, 172, 226);
        Color color2 = new Color(20, 100, 140);
        int w = getWidth();
        int h = getHeight();
        GradientPaint gp = new GradientPaint(0, 0, color1, 0, h, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, w, h);
        
    }
    
    private void initComponents() {
        loginButton = new javax.swing.JButton();
        registerButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        welconText = new javax.swing.JTextPane();
        guest = new javax.swing.JButton();
        highScore = new javax.swing.JButton();

        loginButton.setBackground(new java.awt.Color(36, 172, 226));
        loginButton.setFont(new java.awt.Font("MV Boli", 0, 14)); // NOI18N
        loginButton.setForeground(new java.awt.Color(255, 255, 255));
        loginButton.setToolTipText("login");
        loginButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        loginButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        loginButton.setLabel("Login");

        loginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                navigateToLogin();
            }
        });
        
        registerButton.setBackground(new java.awt.Color(36, 172, 226));
        registerButton.setFont(new java.awt.Font("MV Boli", 0, 14)); // NOI18N
        registerButton.setForeground(new java.awt.Color(255, 255, 255));
        registerButton.setText("Register");
        registerButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        registerButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        welconText.setEditable(false);
        welconText.setBorder(null);
        welconText.setFont(new java.awt.Font("MV Boli", 1, 36)); // NOI18N
        welconText.setForeground(new java.awt.Color(255, 192, 0));
        welconText.setText("Welcome to Brick Breaker");
        welconText.setToolTipText("");
        welconText.setFocusable(false);
        welconText.setOpaque(false);
        jScrollPane1.setViewportView(welconText);
        jScrollPane1.setBorder(BorderFactory.createEmptyBorder());

        registerButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                registerButton.setBackground(new Color(20, 100, 140));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                registerButton.setBackground(new Color(36, 172, 226));
            }
        });
        registerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                registerButtonActionPerformed();
            }
        });
        
        guest.setBackground(new java.awt.Color(36, 172, 226));
        guest.setFont(new java.awt.Font("MV Boli", 0, 14)); // NOI18N
        guest.setForeground(new java.awt.Color(255, 255, 255));
        guest.setActionCommand("play");
        guest.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        guest.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        guest.setLabel("Play Now");
        guest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guestActionPerformed(evt);
            }
        });

        highScore.setBackground(new java.awt.Color(36, 172, 226));
        highScore.setFont(new java.awt.Font("MV Boli", 0, 14)); // NOI18N
        highScore.setForeground(new java.awt.Color(255, 255, 255));
        highScore.setText("High Scores");
        highScore.setActionCommand("play");
        highScore.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        highScore.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        highScore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                highScoreActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(310, 310, 310)
                        .addComponent(loginButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(301, 301, 301)
                        .addComponent(registerButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(301, 301, 301)
                        .addComponent(guest))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(290, 290, 290)
                        .addComponent(highScore))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(122, 122, 122)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(143, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(117, 117, 117)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(loginButton)
                .addGap(18, 18, 18)
                .addComponent(registerButton)
                .addGap(18, 18, 18)
                .addComponent(guest)
                .addGap(18, 18, 18)
                .addComponent(highScore)
                .addContainerGap(228, Short.MAX_VALUE))
        );

        frame.add(this);
    }
    
    private void registerButtonActionPerformed() {    
        
        frame.remove(this);
        if(App.registrationPanel == null)
            App.createRegistrationPanel();
        App.registrationPanel.show();
    }                                              

    private void guestActionPerformed(java.awt.event.ActionEvent evt) {                                      
        frame.remove(this);
        if(App.gamePanel == null)
            App.createGame();
        frame.add(App.gamePanel);
        frame.revalidate();
        frame.repaint();
    }                                     

    private void highScoreActionPerformed(java.awt.event.ActionEvent evt) {                                          
        frame.remove(this);
        if(App.highScorePanel == null) 
            App.createHighScorePanel();
        
        frame.add(App.highScorePanel);
        // Refresh and repaint
        frame.revalidate();
        frame.repaint();
    }                                         

    private void navigateToLogin() {
        frame.remove(this);

        LoginFrame loginPage = new LoginFrame(frame);
        frame.add(loginPage);
        
        AuthenticationService auth = new Authenticate();
        loginPage.setAuthenticateInstance(auth);
        frame.revalidate();
        frame.repaint();
    }
    
    public void show() {
        
        if(App.user != null) {
            this.remove(loginButton);
            this.remove(registerButton);
        }
        
        frame.add(this);
        frame.revalidate();
        frame.repaint();
    }
}
