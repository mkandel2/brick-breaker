package brickbreaker;

import brickbreaker.GameComponents.Brick;
import brickbreaker.Interfaces.BrickFactory;
import brickbreaker.GameComponents.DefaultBrickFactory;
import brickbreaker.Interfaces.GameRenderer;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GamePanel extends JPanel implements ActionListener {
    public boolean play = false;
    public boolean gameOver = false;
    public ScoringSystem scoringSystem;

    public int totalBricks;
    public int rows = 10;
    public int columns = 14;

    public Timer timer;
    public int delay = 8;

    public int paddle = 310;

    public List<Ball> balls = new ArrayList<>();
    public String font = "MV Boli";
    public JFrame frame;
    public GameMapBuilder gameMap;
    private final GameRenderer gameRenderer;

    public GamePanel(JFrame frame) {
        Color color = Color.decode("#8B4513");
        this.balls.add(new Ball(350, 450, 2, -2, 20, color));
        this.totalBricks = rows * columns;
        this.frame = frame;
        BrickFactory brickfactory = new DefaultBrickFactory();
        gameMap = new GameMapBuilder(rows, columns, brickfactory);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        timer = new Timer(delay, this);
        timer.start();
        addKeyBindings();
        
        scoringSystem = App.scoreSystem;
        gameRenderer = new DefaultGameRenderer();
    }
    
    public GamePanel(JFrame frame, GameRenderer gameRenderer) {
        Color color = Color.decode("#8B4513");
        this.balls.add(new Ball(350, 450, 2, -2, 20, color));
        this.totalBricks = rows * columns;
        this.frame = frame;
        BrickFactory brickfactory = new DefaultBrickFactory();
        gameMap = new GameMapBuilder(rows, columns, brickfactory);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        timer = new Timer(delay, this);
        timer.start();
        addKeyBindings();
        
        scoringSystem = App.scoreSystem;
        this.gameRenderer = gameRenderer;
    }

    
    public List<Ball> getBalls() {
        return balls;
    }

    // Getter for paddle
    public int getPaddle() {
        return paddle;
    }

    // Getter for scoringSystem
    public ScoringSystem getScoringSystem() {
        return scoringSystem;
    }

    // Getter for totalBricks
    public int getTotalBricks() {
        return totalBricks;
    }
    
    private void addKeyBindings() {
        InputMap inputMap = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = getActionMap();

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "moveLeft");
        actionMap.put("moveLeft", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (paddle < 10) {
                    paddle = 10;
                } else {
                    moveLeft();
                }
            }
        });

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "moveRight");
        actionMap.put("moveRight", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (paddle >= 600) {
                    paddle = 600;
                } else {
                    moveRight();
                }
            }
        });
        
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Enter");
        actionMap.put("Enter", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!play || gameOver) {
                    play = true;
                    gameOver = false;
                    Color color = Color.decode("#8B4513");
                    balls.clear();
                    balls.add(new Ball(350, 450, 2, -2, 20, color)); 
                    scoringSystem.resetScore();
                    totalBricks = rows * columns;
                    BrickFactory brickfactory = new DefaultBrickFactory();
                    gameMap = new GameMapBuilder(rows, columns, brickfactory);

                    repaint();
                }
            }
        });
        
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESC");
        JPanel thisPanel = this;
        actionMap.put("ESC", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(thisPanel);
                if(App.mainMenu == null) App.createMainMenu();
                
                App.mainMenu.show();
                frame.revalidate();
                frame.repaint();
            }
        });
    }
    
    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);

        if (gameRenderer != null) {
            gameRenderer.render(this, graphics, balls, paddle, scoringSystem, totalBricks);
        }
    }
    
    public void gameOver(Graphics graphics, String gameOverText, Color color) {
        gameOver = true; // Set the game over flag to true
        graphics.setColor(color);
        graphics.setFont(new Font(font, Font.BOLD, 30));
        graphics.drawString(gameOverText + ", Your Score: " + scoringSystem.getScore(), 190, 300);

        graphics.setFont(new Font(font, Font.BOLD, 20));
        graphics.drawString("Press Enter to Restart or ESC to go back to main Menu", 100, 350);
        
        scoringSystem.checkForNewHighScore();
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (play) {
            Iterator<Ball> iterator = balls.iterator();
            List<Ball> newBallsToAdd = new ArrayList<>();
            while (iterator.hasNext()) {
                Ball ball = iterator.next();
                Vector ballPosition = ball.getPosition();
                Vector ballDirection = ball.getDirection();

                // Check ball and paddle collision
                if (new Rectangle(ballPosition.x, ballPosition.y, ball.radius, ball.radius).intersects(new Rectangle(paddle, 550, 100, 8))) {
                    ballDirection.y = -ballDirection.y;
                    ball.setDirection(ballDirection.x, ballDirection.y);
                }

                for (int i = 0; i < gameMap.map.length; i++) {
                    for (int j = 0; j < gameMap.map[0].length; j++) {
                        Brick brick = gameMap.map[i][j];
                        if (!brick.getIsHit()) {
                            Vector brickPosition = brick.getPosition();
                            
                            Rectangle brickRect = new Rectangle(brickPosition.x, brickPosition.y, brick.getWidth(), brick.getHeight());
                            Rectangle ballRect = new Rectangle(ballPosition.x, ballPosition.y, ball.radius, ball.radius);

                            if (ballRect.intersects(brickRect)) {
                                brick.setIsHit(true);
                                totalBricks--;
                                scoringSystem.brickBroken(brick.getValue());
                                
                                if(brick.hasPowerUp) {
                                    int noOfBallsToSpawn = (int) (Math.random() * 5);
                                    for (int k=0; k<noOfBallsToSpawn; k++) {
                                        boolean positiveXDirection = Math.random() > 0.5;
                                        boolean positiveYDirection = Math.random() > 0.5;
                                        int powerUpSpeed = 3;
                                        int dirX = (positiveXDirection)? powerUpSpeed: -powerUpSpeed;
                                        int dirY = (positiveYDirection)? powerUpSpeed: -powerUpSpeed;
                                        Color color = Color.GRAY;
                                        Ball newBall = new Ball(brickPosition.x, brickPosition.y, dirX, dirY, ball.radius, color);
                                        newBallsToAdd.add(newBall);
                                    }
                                }
                                
                                if (ballPosition.x + 19 <= brickRect.x || ballPosition.x + 1 >= brickRect.x + brickRect.width) {
                                    ballDirection.x = -ballDirection.x;
                                } else {
                                    ballDirection.y = -ballDirection.y;
                                }
                            }
                        }
                    }
                }
                ball.setDirection(ballDirection.x, ballDirection.y);
                ball.move();
            }
            balls.addAll(newBallsToAdd);
        }

        repaint();
    }

    public void moveRight() {
        play = true;
        paddle += 50;
    }

    public void moveLeft() {
        play = true;
        paddle -= 50;
    }
    
    public void show() {
        frame.add(this);
        frame.revalidate();
        frame.repaint();
    }
 
}
