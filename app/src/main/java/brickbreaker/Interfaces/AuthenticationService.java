
package brickbreaker.Interfaces;

public interface AuthenticationService {
    boolean login(String email, String password);
}