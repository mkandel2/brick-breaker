package brickbreaker.Interfaces;

import brickbreaker.GameComponents.Brick;

public interface BrickFactory {
    Brick createBrick(int x, int y, int width, int height, Boolean hasPowerup);
}
