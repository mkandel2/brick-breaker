
package brickbreaker.Interfaces;

public interface GameObserver {
    void onGameOver(int finalScore);
    void onGameWin(int finalScore);
}
