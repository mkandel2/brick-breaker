
package brickbreaker.Interfaces;

import brickbreaker.Ball;
import brickbreaker.GamePanel;
import brickbreaker.ScoringSystem;
import java.awt.Graphics;
import java.util.List;

public interface GameRenderer {
    void render(
        GamePanel gamePanel,
        Graphics graphics,
        List<Ball> balls,
        int paddle,
        ScoringSystem scoringSystem,
        int totalBricks
    );
}