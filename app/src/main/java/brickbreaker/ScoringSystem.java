package brickbreaker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ScoringSystem {
    private int score;          // Current Score
    private int highScore;      // Highest Score

    public ScoringSystem() {
        this.score = 0;
        this.setHighScore();
    }

    public void setHighScore() {
        this.highScore = loadHighScore();
    }
    
    private int loadHighScore() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        if (App.user == null) return 0;
        try {
            // Establish a database connection
            connection = DriverManager.getConnection(App.DB_URL, App.DB_USER, App.DB_PASSWORD);

            // Define the SQL query to retrieve the high score
            String sql = "SELECT MAX(score) AS high_score FROM scores WHERE user_id=? GROUP BY user_id";

            // Create a prepared statement
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, App.user.id);
            // Execute the query and retrieve the result
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                // Read the high score value from the result set
                return resultSet.getInt("high_score");
            }
        } catch (SQLException e) {
            System.err.println("Database Error: " + e.getMessage());
        } finally {
            // Close database resources in reverse order of acquisition
            try {
                if (resultSet != null) resultSet.close();
                if (preparedStatement != null) preparedStatement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                System.err.println("Error closing database resources: " + e.getMessage());
            }
        }

        // Return a default value if high score retrieval fails
        return 0;
    }
    
    public List<Integer> getScores() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Integer> scores = new ArrayList<>();

        if (App.user == null) return scores;
        
        try {
            // Establish a database connection
            connection = DriverManager.getConnection(App.DB_URL, App.DB_USER, App.DB_PASSWORD);

            // Define the SQL query to retrieve scores for the user
            String sql = "SELECT score FROM scores WHERE user_id=? ORDER BY id DESC";

            // Create a prepared statement
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, App.user.id);

            // Execute the query and retrieve the results
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                // Read each score from the result set and add it to the list
                int score = resultSet.getInt("score");
                scores.add(score);
            }
        } catch (SQLException e) {
            System.err.println("Database Error: " + e.getMessage());
        } finally {
            // Close database resources in reverse order of acquisition
            try {
                if (resultSet != null) resultSet.close();
                if (preparedStatement != null) preparedStatement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                System.err.println("Error closing database resources: " + e.getMessage());
            }
        }

        // Return the list of scores
        return scores;
    }


    private void saveHighScore() {
        if(App.user == null) return;
        try (Connection connection = DriverManager.getConnection(App.DB_URL, App.DB_USER, App.DB_PASSWORD)) {
            String sql = "INSERT INTO scores (user_id, score) VALUES (?, ?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, App.user.id);
                preparedStatement.setInt(2, score);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            System.err.println("Database Error: " + e.getMessage());
        }
    }

    // Add point to each broken brick
    public void brickBroken(int points) {
        score += points;
    }

    // Compare Current Score and Hgihest Score
    public void checkForNewHighScore() {
        if (score > highScore) {
            highScore = score;
            this.saveHighScore();
        }
    }

    // Reset score
    public void resetScore() {
        score = 0;
    }

    // Getters
    public int getScore() {
        return score;
    }

    public int getHighScore() {
        return highScore;
    }
}


