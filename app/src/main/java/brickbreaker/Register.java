
package brickbreaker;

import Auth.Authenticate;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Register extends JPanel {
    
    private JTextField usernameField;
    private JTextField emailField;
    private JPasswordField passwordField;
    private JButton registerButton;
    private JButton backButton;
    private JLabel headerLabel, usernameLabel, emailLabel, passwordLabel;
    private JFrame parentFrame;
    private Color buttonColor = new Color(36, 172, 226);
    private Color hoverButtonColor = new Color(26, 152, 206);
    private Font textFont = new Font("MV Boli", Font.PLAIN, 14);
    private Font labelFont = new Font("MV Boli", Font.BOLD, 16);
    private Font headerFont = new Font("MV Boli", Font.BOLD, 24);
    private JLabel usernameErrorLabel = new JLabel("");
    private JLabel emailErrorLabel = new JLabel("");
    private JLabel passwordErrorLabel = new JLabel("");
    private Authenticate fileAuth = new Authenticate();

    public Register(JFrame parentFrame) {
        this.parentFrame = parentFrame;
        initComponents();
        setBackground(new Color(240, 240, 255)); // Light blue background
    }

    private void initComponents() {

        headerLabel = new JLabel("Registration");
        headerLabel.setFont(headerFont);
        headerLabel.setForeground(buttonColor);

        usernameLabel = new JLabel("Username:");
        emailLabel = new JLabel("Email:");
        passwordLabel = new JLabel("Password:");

        usernameField = new JTextField(20);
        emailField = new JTextField(20);
        passwordField = new JPasswordField(20);

        registerButton = new JButton("Register");
        backButton = new JButton("Back");

        usernameLabel.setFont(labelFont);
        emailLabel.setFont(labelFont);
        passwordLabel.setFont(labelFont);
        usernameField.setFont(textFont);
        emailField.setFont(textFont);
        passwordField.setFont(textFont);
        registerButton.setFont(new Font("MV Boli", Font.BOLD, 16));
        backButton.setFont(new Font("MV Boli", Font.BOLD, 16));

        registerButton.setBackground(buttonColor);
        registerButton.setForeground(Color.WHITE);
        backButton.setBackground(buttonColor);
        backButton.setForeground(Color.WHITE);

        registerButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
        backButton.setCursor(new Cursor(Cursor.HAND_CURSOR));

        usernameErrorLabel.setPreferredSize(new Dimension(200, 20));
        emailErrorLabel.setPreferredSize(new Dimension(200, 20));
        passwordErrorLabel.setPreferredSize(new Dimension(200, 20));
        usernameErrorLabel.setForeground(Color.RED);
        emailErrorLabel.setForeground(Color.RED);
        passwordErrorLabel.setForeground(Color.RED);

        // Hover effects for buttons
        registerButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                registerButton.setBackground(hoverButtonColor);
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                registerButton.setBackground(buttonColor);
            }
        });

        backButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                backButton.setBackground(hoverButtonColor);
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                backButton.setBackground(buttonColor);
            }
        });

        registerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                handleRegistration();
            }
        });

        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                handleBack();
            }
        });

        // Layout with padding and alignment
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10, 10, 10, 10);

        gbc.gridwidth = 2;
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(headerLabel, gbc);

        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 1;
        add(usernameLabel, gbc);

        gbc.gridx = 1;
        add(usernameField, gbc);

        gbc.gridx = 2;
        add(usernameErrorLabel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        add(emailLabel, gbc);

        gbc.gridx = 1;
        add(emailField, gbc);

        gbc.gridx = 2;
        add(emailErrorLabel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        add(passwordLabel, gbc);

        gbc.gridx = 1;
        add(passwordField, gbc);

        gbc.gridx = 2;
        add(passwordErrorLabel, gbc);

        gbc.gridwidth = 2;
        gbc.gridx = 0;
        gbc.gridy = 4;
        add(registerButton, gbc);

        gbc.gridy = 5;
        add(backButton, gbc);

        setBorder(BorderFactory.createEmptyBorder(50, 50, 50, 50));
    }

    private void handleRegistration() {
        // Reset error labels
        usernameErrorLabel.setText("");
        emailErrorLabel.setText("");
        passwordErrorLabel.setText("");

        String username = usernameField.getText();
        String email = emailField.getText();
        String password = new String(passwordField.getPassword());
        boolean error = false;

        // Simple validation
        if (username.isEmpty()) {
            usernameErrorLabel.setText("Username is required!");
            error = true;
        }

        if (email.isEmpty()) {
            emailErrorLabel.setText("Email is required!");
            error = true;
        } else if (!isValidEmail(email)) {
            emailErrorLabel.setText("Invalid email format!");
            error = true;
        }

        if (password.isEmpty()) {
            passwordErrorLabel.setText("Password is required!");
            error = true;
        }

        if (error) return;

        // You can implement the registration logic here
        // For example, call a method to register the user
        try (Connection connection = DriverManager.getConnection(App.DB_URL, App.DB_USER, App.DB_PASSWORD)) {
            // Define the SQL query to insert registration data
            String sql = "INSERT INTO users (username, email, password) VALUES (?, ?, ?)";

            // Create a prepared statement
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, username);
                preparedStatement.setString(2, email);
                preparedStatement.setString(3, password);

                // Execute the insert query
                int rowsInserted = preparedStatement.executeUpdate();

                if (rowsInserted > 0) {
                    JOptionPane.showMessageDialog(this, "Registration successful! You may now Login", "Success", JOptionPane.INFORMATION_MESSAGE);
                    handleBack(); // Go back to the login screen after successful registration
                } else {
                    JOptionPane.showMessageDialog(this, "Registration failed. Please try again.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Database error. Registration failed. Please try again.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void handleBack() {
        parentFrame.remove(this);
        App.mainMenu.show();
    }
    
    public void show() {
        App.frame.add(this);
        App.frame.revalidate();
        App.frame.repaint();
    }

    public static boolean isValidEmail(String email) {
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
