
package Auth;

import brickbreaker.App;
import brickbreaker.Interfaces.AuthenticationService;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Authenticate implements AuthenticationService {
    
    public boolean login(String email, String password) {
        try (Connection connection = DriverManager.getConnection(App.DB_URL, App.DB_USER, App.DB_PASSWORD)) {
            String sql = "SELECT * FROM users WHERE email = ? AND password = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, email);
                preparedStatement.setString(2, password);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    // User exists, return user details
                    int id = resultSet.getInt("id");
                    String name = resultSet.getString("username");
                    String fetchedEmail = resultSet.getString("email");
                    User user = new User(id, name, fetchedEmail);
                    App.user = user;
                    return true;
                }
            }
        } catch (SQLException e) {
            System.err.println("Database Error: " + e.getMessage());
        }
        return false; 
    }


}
