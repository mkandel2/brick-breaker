package brickbreaker;

import brickbreaker.GameComponents.Brick;
import brickbreaker.GameComponents.DefaultBrickFactory;
import brickbreaker.Interfaces.GameRenderer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class GamePanelTest {

    @Mock
    private JFrame frame;

    @Mock
    private ScoringSystem scoringSystem;

    @Mock
    private GameRenderer gameRenderer;

    private GamePanel gamePanel;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        gamePanel = new GamePanel(frame, gameRenderer);
        gamePanel.scoringSystem = scoringSystem;
    }

    @Test
    public void testInitialization() {
        assertEquals(1, gamePanel.getBalls().size());
        assertFalse(gamePanel.play);
        assertFalse(gamePanel.gameOver);
        assertNotNull(gamePanel.timer);
    }

    @Test
    public void testMoveRight() {
        gamePanel.moveRight();
        assertTrue(gamePanel.play);
    }

    @Test
    public void testMoveLeft() {
        gamePanel.moveLeft();
        assertTrue(gamePanel.play);
    }

    @Test
    public void testActionPerformed() {
        Ball mockBall = mock(Ball.class);
        when(mockBall.getRadius()).thenReturn(20);
        when(mockBall.getPosition()).thenReturn(new Vector(100, 100));
        when(mockBall.getColor()).thenReturn(Color.decode("#8B4513"));
        when(mockBall.getDirection()).thenReturn(new Vector(-1, 1));
        
        List<Ball> balls = new ArrayList<>();
        balls.add(mockBall);
        gamePanel.balls = balls;
         gamePanel.play = true;
        // Create a mock Brick
        Brick mockBrick = mock(Brick.class);
        when(mockBrick.getIsHit()).thenReturn(false);
        when(mockBrick.getPosition()).thenReturn(new Vector(100, 100));
        when(mockBrick.getWidth()).thenReturn(50);
        when(mockBrick.getHeight()).thenReturn(20);

        // Create a mock GameMapBuilder and set the map to contain the mock brick
        Brick[][] mockMap = new Brick[][]{{mockBrick}};
        gamePanel.gameMap = new GameMapBuilder(1, 1, new DefaultBrickFactory());
        gamePanel.gameMap.map = mockMap;

        // Create a mock ActionEvent
        ActionEvent mockActionEvent = mock(ActionEvent.class);

        // Call actionPerformed method with the mock ActionEvent
        gamePanel.actionPerformed(mockActionEvent);

        // Verify that expected interactions with mocks occurred

        // Verify that getPosition and getDirection are called on mockBall
        verify(mockBall, atLeast(1)).getPosition();
        verify(mockBall, atLeast(1)).move();

        // Verify that getIsHit, getPosition, getWidth, and getHeight are called on mockBrick
        verify(mockBrick, atLeast(1)).getIsHit();
        verify(mockBrick, atLeast(1)).getPosition();
        verify(mockBrick, atLeast(1)).getWidth();
        verify(mockBrick, atLeast(1)).getHeight();

    }
    
}