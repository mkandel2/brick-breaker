
package brickbreaker;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.awt.Color;
import java.awt.Graphics;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BallTest {

    @Mock
    private Graphics mockGraphics;

    private Ball ball;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ball = new Ball(100, 100, 2, 2, 10, Color.RED);
    }

    @Test
    public void testGetPosition() {
        assertEquals(100, ball.getPosition().x);
        assertEquals(100, ball.getPosition().y);
    }

    @Test
    public void testGetRadius() {
        assertEquals(10, ball.getRadius());
    }

    @Test
    public void testGetColor() {
        assertEquals(Color.RED, ball.getColor());
    }

    @Test
    public void testGetDirection() {
        assertEquals(2, ball.getDirection().x);
        assertEquals(2, ball.getDirection().y);
    }

    @Test
    public void testSetPosition() {
        ball.setPosition(200, 200);
        assertEquals(200, ball.getPosition().x);
        assertEquals(200, ball.getPosition().y);
    }

    @Test
    public void testSetDirection() {
        ball.setDirection(3, 3);
        assertEquals(3, ball.getDirection().x);
        assertEquals(3, ball.getDirection().y);
    }

    @Test
    public void testMove() {
        ball.move();
        assertEquals(102, ball.getPosition().x);
        assertEquals(102, ball.getPosition().y);
    }

    @Test
    public void testCheckForWallCollisions() {
        // Simulate a collision with the right wall
        ball.setPosition(680, 100);
        ball.checkForWallCollisions();

        assertEquals(670, ball.getPosition().x);
        assertEquals(100, ball.getPosition().y);
        
        assertEquals(-2, ball.getDirection().x);
        assertEquals(2, ball.getDirection().y);
        
        // Simulate a collision with the top wall
        ball.setPosition(100, 0);
        ball.checkForWallCollisions();
        
        assertEquals(100, ball.getPosition().x);
        assertEquals(0, ball.getPosition().y);
        
        assertEquals(2, ball.getDirection().y);
        

        // Simulate a collision with the left wall
        ball.setPosition(-10, 100);
        ball.checkForWallCollisions();
        
        assertEquals(0, ball.getPosition().x);
        assertEquals(100, ball.getPosition().y);
        
        assertEquals(2, ball.getDirection().x);
        assertEquals(2, ball.getDirection().y);
        
    }

    @Test
    public void testDraw() {
        // Mock the Graphics object
        ball.draw(mockGraphics);

        // Verify that setColor and fillOval are called on the mockGraphics
        verify(mockGraphics).setColor(Color.RED);
        verify(mockGraphics).fillOval(100, 100, 10, 10);
    }
}

