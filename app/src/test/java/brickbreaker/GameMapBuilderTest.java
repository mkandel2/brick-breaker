
package brickbreaker;

import brickbreaker.GameComponents.Brick;
import brickbreaker.Interfaces.BrickFactory;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GameMapBuilderTest {

    private BrickFactory brickFactory;
    private GameMapBuilder gameMapBuilder;

    @BeforeEach
    public void setUp() {
        // Initialize the BrickFactory mock
        brickFactory = mock(BrickFactory.class);
        gameMapBuilder = new GameMapBuilder(3, 4, brickFactory);
    }

    @Test
    public void testMapInitialization() {
        // Define your test case
        Brick[][] map = gameMapBuilder.map;

        // Ensure that the map dimensions match the constructor parameters
        assertEquals(3, map.length);
        assertEquals(4, map[0].length);

        // Verify that the brickFactory.createBrick method was called appropriately
        verify(brickFactory, atLeastOnce()).createBrick(anyInt(), anyInt(), anyInt(), anyInt(), anyBoolean());
    }

}
