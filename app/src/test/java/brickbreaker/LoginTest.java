
package brickbreaker;

import static org.junit.jupiter.api.Assertions.*;

import javax.swing.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LoginTest {

    private LoginFrame loginFrame;
    private JFrame parentFrame;

    @BeforeEach
    public void setUp() {
        parentFrame = new JFrame();
        loginFrame = new LoginFrame(parentFrame);
    }

   
    @Test
    public void testIsValidEmail() {
        assertTrue(LoginFrame.isValidEmail("valid@example.com"));
        assertFalse(LoginFrame.isValidEmail("invalid-email"));
    }

}
